import pandas as pd
import language_tool_python


def read_dataset_cisi():
    ### Processing DOCUMENTS
    doc_set = {}
    doc_id = ""
    doc_text = ""
    with open(r'C:\Users\reda\Desktop\CISI\CISI.ALL') as f:
        lines = ""
        for l in f.readlines():
            lines += "\n" + l.strip() if l.startswith(".") else " " + l.strip()
        lines = lines.lstrip("\n").split("\n")
    doc_count = 0
    for l in lines:
        if l.startswith(".I"):
            doc_id = int(l.split(" ")[1].strip()) - 1
        elif l.startswith(".X"):
            doc_set[doc_id] = doc_text.lstrip(" ")
            doc_id = ""
            doc_text = ""
        else:
            doc_text += l.strip()[3:] + " "  # The first 3 characters of a line can be ignored.

    ### Processing QUERIES
    with open(r'C:\Users\reda\Desktop\CISI\CISI.QRY') as f:
        lines = ""
        for l in f.readlines():
            lines += "\n" + l.strip() if l.startswith(".") else " " + l.strip()
        lines = lines.lstrip("\n").split("\n")

    qry_set = {}
    qry_id = ""
    for l in lines:
        if l.startswith(".I"):
            qry_id = int(l.split(" ")[1].strip()) - 1
        elif l.startswith(".W"):
            qry_set[qry_id] = l.strip()[3:]
            qry_id = ""

    ### Processing QRELS
    rel_set = {}
    with open(r"C:\Users\reda\Desktop\CISI\CISI.REL") as f:
        for l in f.readlines():
            qry_id = int(l.lstrip(" ").strip("\n").split("\t")[0].split(" ")[0]) - 1
            doc_id = int(l.lstrip(" ").strip("\n").split("\t")[0].split(" ")[-1]) - 1
            if qry_id in rel_set:
                rel_set[qry_id].append(doc_id)
            else:
                rel_set[qry_id] = [doc_id]

    return doc_set, qry_set, rel_set


def read_dataset_cacm():
    ### Processing DOCUMENTS
    doc_set = {}
    doc_id = ""
    doc_text = ""
    with open(r'C:\Users\reda\Desktop\cacm\cacm.all') as f:
        lines = ""
        for l in f.readlines():
            lines += "\n" + l.strip() if l.startswith(".") else " " + l.strip()
        lines = lines.lstrip("\n").split("\n")
    doc_count = 0
    for l in lines:
        if l.startswith(".I"):
            doc_id = int(l.split(" ")[1].strip()) - 1
        elif l.startswith(".X"):
            doc_set[doc_id] = doc_text.lstrip(" ")
            doc_id = ""
            doc_text = ""
        else:
            doc_text += l.strip()[3:] + " "  # The first 3 characters of a line can be ignored.

    ### Processing QUERIES
    with open(r'C:\Users\reda\Desktop\cacm\query.text') as f:
        lines = ""
        for l in f.readlines():
            lines += "\n" + l.strip() if l.startswith(".") else " " + l.strip()
        lines = lines.lstrip("\n").split("\n")

    qry_set = {}
    qry_id = ""
    for l in lines:
        if l.startswith(".I"):
            qry_id = int(l.split(" ")[1].strip()) - 1
        elif l.startswith(".W"):
            qry_set[qry_id] = l.strip()[3:]
            qry_id = ""

    ### Processing QRELS
    rel_set = {}
    with open(r'C:\Users\reda\Desktop\cacm\qrels.text') as f:
        for l in f.readlines():
            qry_id = int(l.lstrip(" ").strip("\n").split(' ')[0]) - 1
            doc_id = int(l.lstrip(" ").strip("\n").split(' ')[1]) - 1
            if qry_id in rel_set:
                rel_set[qry_id].append(doc_id)
            else:
                rel_set[qry_id] = [doc_id]

    return doc_set, qry_set, rel_set


def prepare_dataset(name='cisi'):
    if name.lower() == 'cisi':
        doc_set, qry_set, rel_set = read_dataset_cisi()
    else:
        doc_set, qry_set, rel_set = read_dataset_cacm()

    qry_values = qry_set.values()
    df = pd.DataFrame({'query': qry_values, 'relations': pd.Series(qry_values).apply(lambda x: None)})
    for idx in qry_set.keys():
        if idx in rel_set.keys():
            relations = ' '.join(pd.Series(rel_set[idx]).astype(str))
            df.at[idx, 'relations'] = relations

    docs = pd.Series(doc_set.values())

    return docs, df


tool = language_tool_python.LanguageTool('en-US')


def stringify_tool_match(match):
    s = ''
    if match.message:
        s += '\nMessage: {}'.format(match.message)
    if match.replacements:
        s += '\nSuggestion: {}'.format('; '.join(match.replacements))
    s += '\n{}\n{}'.format(match.context, ' ' * match.offsetInContext + '^' * match.errorLength)
    return s
