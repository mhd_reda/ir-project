import pycountry
import string
import dateparser
import re
import spacy
from spacy.matcher import Matcher
from spacy.tokenizer import Tokenizer

nlp = spacy.load("en_core_web_sm")

DATE = 'DATE'
GPE = 'GPE'
infix_re = re.compile(r'''[-/,]''')
nlp.tokenizer = Tokenizer(nlp.vocab, infix_finditer=infix_re.finditer)
# for the token pattern 1st, 22nd, 15th etc
IS_REGEX_MATCH = {"TEXT": {"REGEX": r"^\d{1,2}(?:[stndrh]){2}?$"}}

# MM/DD/YYYY and YYYY/MM/DD
pattern_1 = [{'IS_DIGIT': True}, {'ORTH': '/'}, {'IS_DIGIT': True}, {'ORTH': '/'}, {'IS_DIGIT': True}]
# MM-DD-YYYY and YYYY-MM-DD
pattern_2 = [{'IS_DIGIT': True}, {'ORTH': '-'}, {'IS_DIGIT': True}, {'ORTH': '-'}, {'IS_DIGIT': True}]
# dates of the form 10-Aug-2018
pattern_3 = [{'IS_DIGIT': True}, {'ORTH': '-'}, {'IS_ALPHA': True}, {'ORTH': '-'}, {'IS_DIGIT': True}]
# dates of the form Aug-10-2018
pattern_4 = [{'IS_ALPHA': True}, {'ORTH': '-'}, {'IS_DIGIT': True}, {'ORTH': '-'}, {'IS_DIGIT': True}]
# dates of the form 10th August, 2018
pattern_5 = [IS_REGEX_MATCH, {'IS_ALPHA': True}, {'ORTH': ',', 'OP': '?'}, {'IS_DIGIT': True}]
# dates of the form August 10th, 2018
pattern_6 = [{'IS_ALPHA': True}, IS_REGEX_MATCH, {'ORTH': ',', 'OP': '?'}, {'IS_DIGIT': True}]
# dates of the form August 10, 2018
pattern_7 = [{'IS_ALPHA': True}, {'IS_DIGIT': True}, {'ORTH': ',', 'OP': '?'}, {'IS_DIGIT': True}]


def add_date_ent(_, doc, i, matches):
    match_id, start, end = matches[i]
    entity = (nlp.vocab.strings[DATE], start, end)
    doc.ents += (entity,)


def remove_punctuation(text: str) -> str:
    corrected = re.sub(r'[%s]' % re.escape(string.punctuation), r'', text)
    return corrected


def remove_whitespace_duplicate(text: str) -> str:
    corrected = re.sub(r"//t", r"\t", text)
    corrected = re.sub(r"( )\1+", r"\1", corrected)
    corrected = re.sub(r"(\n)\1+", r"\1", corrected)
    corrected = re.sub(r"(\r)\1+", r"\1", corrected)
    corrected = re.sub(r"(\t)\1+", r"\1", corrected)
    return corrected.strip()


def preprocess(text: str) -> str:
    # normalization
    doc = nlp(text)
    new_text = ' '.join([token.lemma_ for token in doc if not nlp.vocab[token.text].is_stop])
    matcher = Matcher(nlp.vocab)
    matcher.add('Dates', [pattern_1, pattern_2, pattern_3, pattern_4, pattern_5, pattern_6], on_match=add_date_ent)
    matcher(doc)
    se = set()
    for ent in doc.ents:
        if ent.text not in se:
            se.add(ent.text)
            if ent.label_ == DATE:
                date = dateparser.parse(ent.text)
                if date:
                    new_text = new_text.replace(ent.text, date.strftime('%Y-%m-%d'))
            if ent.label_ == GPE:
                try:
                    country = pycountry.countries.search_fuzzy(ent.text)[0]
                    new_text = new_text.replace(ent.text, country.name)
                except LookupError:
                    pass
    res = remove_punctuation(new_text)
    res = remove_whitespace_duplicate(res).strip()
    return res.lower()
