import pickle as pkl
import numpy as np
import pandas as pd

from .utils import prepare_dataset
from rank_bm25 import BM25Okapi
from .core import preprocess

docs, df = prepare_dataset()
other_docs, other_df = prepare_dataset('cacm')


def rank_dataset(name='cisi'):
    if name.lower() == 'cisi':
        processed_docs = docs.apply(preprocess)
    else:
        processed_docs = other_docs.apply(preprocess)
    tokenized_corpus = processed_docs.str.split(' ')
    bm25 = BM25Okapi(tokenized_corpus)
    return bm25


with open("bm25_cisi.pkl", 'rb') as f:
    bm25_cisi_engine = pkl.load(f)

with open('bm25_cacm.pkl', 'rb') as f:
    bm25_cacm_engine = pkl.load(f)

with open(f'cisi_clusters/cisi_kmeans.pkl', 'rb') as f:
    cisi_kmeans = pkl.load(f)

with open(f'cisi_clusters/cisi_vectorizer.pkl', 'rb') as f:
    cisi_vectorizer = pkl.load(f)

cisi_kmeans_dict = dict()

for idx in range(3):
    with open(f'cisi_clusters/cluster_{idx}_cisi_corpus.pkl', 'rb') as f:
        _corpus = pkl.load(f)
    with open(f'cisi_clusters/bm25_cisi_{idx}.pkl', 'rb') as f:
        _index = pkl.load(f)

    cisi_kmeans_dict[idx] = {
        'corpus': _corpus,
        'index': _index,
    }

# bm25_engine = rank_cisi()

with open(f'cacm_clusters/cacm_kmeans.pkl', 'rb') as f:
    cacm_kmeans = pkl.load(f)

with open(f'cacm_clusters/cacm_vectorizer.pkl', 'rb') as f:
    cacm_vectorizer = pkl.load(f)

cacm_kmeans_dict = dict()

for idx in range(2):
    with open(f'cacm_clusters/cluster_{idx}_cacm_corpus.pkl', 'rb') as f:
        _corpus = pkl.load(f)
    with open(f'cacm_clusters/bm25_cacm_{idx}.pkl', 'rb') as f:
        _index = pkl.load(f)

    cacm_kmeans_dict[idx] = {
        'corpus': _corpus,
        'index': _index,
    }


def search(query, dataset_name='cisi', n=15):
    if dataset_name.lower() == 'cisi':
        scores = bm25_cisi_engine.get_scores(preprocess(query).split(' '))
    else:
        scores = bm25_cacm_engine.get_scores(preprocess(query).split(' '))
    ind = np.argpartition(scores, -n)[-n:]
    if dataset_name.lower() == 'cisi':
        ret_docs = docs.iloc[ind]
    else:
        ret_docs = other_docs.iloc[ind]
    return ind, scores[ind], ret_docs


def predict_cluster_idx(query, dataset_name='cisi'):
    if dataset_name.lower() == 'cisi':
        return cisi_kmeans.predict(cisi_vectorizer.transform(np.array([preprocess(query)]))).flatten()[0]
    return cacm_kmeans.predict(cacm_vectorizer.transform(np.array([preprocess(query)]))).flatten()[0]


def search_in_clusters(query, cluster_idx, dataset_name='cisi', n=15):
    if dataset_name.lower() == 'cisi':
        index = cisi_kmeans_dict[cluster_idx]['index']
        corpus = cisi_kmeans_dict[cluster_idx]['corpus']

        scores = np.zeros(docs.shape)
        scores[corpus.index] = index.get_scores(query.split(' '))

        if n > 0:
            ind = np.argpartition(scores, -n)[-n:]

            return ind, scores[ind], docs.iloc[ind]

        return corpus.index, scores, docs

    index = cacm_kmeans_dict[cluster_idx]['index']
    corpus = cacm_kmeans_dict[cluster_idx]['corpus']

    scores = np.zeros(other_docs.shape)
    scores[corpus.index] = index.get_scores(query.split(' '))

    if n > 0:
        ind = np.argpartition(scores, -n)[-n:]

        return ind, scores[ind], other_docs.iloc[ind]

    return corpus.index, scores, other_docs


def calc_precision_at_k(k, relations, queries_scores):
    ap = 0
    count = 0
    for relevant, scores in zip(relations, queries_scores):
        if relevant is not None:
            retrieved = np.argpartition(scores, -k)[-k:]
            retrieved = pd.Series(scores[retrieved], index=retrieved).sort_values(ascending=False)
            relevant = set(pd.Series(relevant.split(' ')).astype(int))
            if retrieved.index[-1] in relevant:
                retrieved = set(retrieved.index)
                numerator = len(relevant.intersection(retrieved))
                precision = numerator / len(retrieved)
                ap += precision
                count += 1
    if count:
        return ap / count
    return 0


def performance_report(relations, queries_scores):
    overall_recall = 0
    overall_precision = 0
    count = 0

    for relevant, scores in zip(relations, queries_scores):
        if relevant is not None:
            retrieved = set(np.argwhere(scores).flatten())
            relevant = set(pd.Series(relevant.split(' ')).astype(int))
            numerator = len(relevant.intersection(retrieved))
            recall = numerator / len(relevant)
            precision = numerator / len(retrieved)

            overall_recall += recall
            overall_precision += precision
            count += 1

    overall_precision = overall_precision / count
    overall_recall = overall_recall / count

    _map = sum((calc_precision_at_k(k, relations, queries_scores) for k in range(1, count + 1))) / count

    mrr = 0
    count = 0
    for relevant, scores in zip(relations, queries_scores):
        if relevant is not None:
            count += 1
            retrieved = np.argwhere(scores).flatten()
            retrieved = pd.Series(scores[retrieved], index=retrieved).sort_values(ascending=False)
            relevant = set(pd.Series(relevant.split(' ')).astype(int))
            rank = 1e9
            for relevant_query in relevant:
                res = list(np.where(retrieved.index == relevant_query)[0])
                if res:
                    rank = min(rank, res[0] + 1)
            mrr += 1 / rank
    mrr = mrr / count

    return {
        'recall': overall_recall,
        'precision': overall_precision,
        'precision@10': calc_precision_at_k(10, relations, queries_scores),
        'MAP': _map,
        'MRR': mrr
    }
