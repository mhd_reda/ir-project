from django.urls import path
from . import views

urlpatterns = [

    path('index/', views.index, name='blog-index'),
    path(r'result/', views.result, name='blog-result'),
    path(r'cisi-performance/', views.performance_report_cisi_view, name='cisi-performance'),
    path(r'cisi-clusters-performance/', views.performance_cluster_report_cisi_view,
         name='cisi-clusters-performance'),
    path(r'cacm-performance/', views.performance_report_cacm_view, name='cacm-performance'),
    path(r'cacm-clusters-performance/', views.performance_cluster_report_cacm_view,
         name='cacm-clusters-performance'),
    # path('evaluation/', views.evaluation, name='eval')
]
