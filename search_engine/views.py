import language_tool_python
from collections import namedtuple

from django.core.handlers.wsgi import WSGIRequest
from django.http import JsonResponse
from django.shortcuts import render
from .core import preprocess
from .engine import (search, performance_report,
                     search_in_clusters, predict_cluster_idx,
                     df, other_df, bm25_cisi_engine, bm25_cacm_engine)
from .utils import tool


def index(request):
    return render(request, 'index.html', context={'choices': ['CISI', 'CACM'], 'default_dataset': 'CISI'})


def result(request: WSGIRequest):
    query_dict = request.GET
    q = query_dict['q']
    matches = tool.check(q)
    corrected_query = language_tool_python.utils.correct(q, matches) if matches else ''
    ind, scores, docs = search(q, dataset_name=query_dict.get('dataset', 'CISI'))
    SearchResult = namedtuple('SearchResult', ['name', 'value', 'text'])
    res = [SearchResult(idx, score, doc) for idx, score, doc in zip(ind, scores, docs)]
    res = sorted(res, key=lambda x: x[1], reverse=True)
    return render(request, 'result.html', context={'value': q,
                                                   'dataset': query_dict.get('dataset', 'CISI'),
                                                   'method': 'BM25',
                                                   'correct': corrected_query,
                                                   'dictionary': res})


def result_clusters(request: WSGIRequest):
    query_dict = request.GET
    q = query_dict['q']
    matches = tool.check(q)
    corrected_query = language_tool_python.utils.correct(q, matches) if matches else ''

    ind, scores, docs = search_in_clusters(q, cluster_idx=predict_cluster_idx(q,
                                                                              dataset_name=query_dict.get('dataset',
                                                                                                          'CISI')),
                                           dataset_name=query_dict.get('dataset', 'CISI'))
    SearchResult = namedtuple('SearchResult', ['name', 'value', 'text'])
    res = [SearchResult(idx, score, doc) for idx, score, doc in zip(ind, scores, docs)]
    res = sorted(res, key=lambda x: x[1], reverse=True)
    return render(request, 'result.html', context={'value': q,
                                                   'dataset': query_dict.get('dataset', 'CISI'),
                                                   'method': 'BM25',
                                                   'correct': corrected_query,
                                                   'dictionary': res})


def performance_cluster_report_cisi_view(request):
    data = performance_report(df['relations'],
                              df['query'].apply(preprocess).apply(lambda x:
                                                                  search_in_clusters(x,
                                                                                     n=-1,
                                                                                     cluster_idx=predict_cluster_idx(x, dataset_name='cisi'),
                                                                                     dataset_name='cisi',
                                                                                     )[1]))
    return JsonResponse(data)


def performance_report_cisi_view(request):
    data = performance_report(df['relations'],
                              df['query'].apply(preprocess).str.split(' ').apply(bm25_cisi_engine.get_scores))
    return JsonResponse(data)


def performance_report_cacm_view(request):
    data = performance_report(other_df['relations'],
                              other_df['query'].apply(preprocess).str.split(' ').apply(bm25_cacm_engine.get_scores))
    return JsonResponse(data)


def performance_cluster_report_cacm_view(request):
    data = performance_report(other_df['relations'],
                              other_df['query'].apply(preprocess).apply(lambda x:
                                                                        search_in_clusters(x,
                                                                                           n=-1,
                                                                                           cluster_idx=
                                                                                           predict_cluster_idx(x,
                                                                                                               dataset_name='cacm'),
                                                                                           dataset_name='cacm',
                                                                                           )[1]))
    return JsonResponse(data)
